import tensorflow as tf
from tensorflow.examples.tutorials.mnist import input_data
import os

cwd = os.getcwd()

# Uncomment this if you need to download the data.
mnist = input_data.read_data_sets(cwd, one_hot=True)

# Construct the neural network model.
n_nodes_hl1 = 500
n_nodes_hl2 = 500
n_nodes_hl3 = 500

n_classes = 10
batch_size = 100

# Create a placeholder for the input data (flattened image).
# The images are 28 pixels x 28 pixels = 784 total pixels
x = tf.placeholder('float', [None, 784])

# Create a placeholder for the output data.
y = tf.placeholder('float')


def neural_network_model(data):

    # Set up some variables to use in the layer operations.
    hidden_layer_1 = {
        'weights': tf.Variable(tf.random_normal([784, n_nodes_hl1])),
        'biases': tf.Variable(tf.random_normal([n_nodes_hl1]))
    }

    hidden_layer_2 = {
        'weights': tf.Variable(tf.random_normal([n_nodes_hl1, n_nodes_hl2])),
        'biases': tf.Variable(tf.random_normal([n_nodes_hl2]))
    }

    hidden_layer_3 = {
        'weights': tf.Variable(tf.random_normal([n_nodes_hl2, n_nodes_hl3])),
        'biases': tf.Variable(tf.random_normal([n_nodes_hl3]))
    }

    output_layer = {
        'weights': tf.Variable(tf.random_normal([n_nodes_hl3, n_classes])),
        'biases': tf.Variable(tf.random_normal([n_classes]))
    }

    # Perform the operations (i.e. feed the data forward). For each layer,
    #   (weights * data) + bias --> activation (relu, in this case)
    out_layer_1 = tf.add(
        tf.matmul(data, hidden_layer_1['weights']), hidden_layer_1['biases'])
    out_layer_1 = tf.nn.relu(out_layer_1)

    out_layer_2 = tf.add(
        tf.matmul(out_layer_1, hidden_layer_2['weights']),
        hidden_layer_2['biases'])
    out_layer_2 = tf.nn.relu(out_layer_2)

    out_layer_3 = tf.add(
        tf.matmul(out_layer_2, hidden_layer_3['weights']),
        hidden_layer_3['biases'])
    out_layer_3 = tf.nn.relu(out_layer_3)

    final_out = tf.add(
        tf.matmul(out_layer_3, output_layer['weights']),
        output_layer['biases'])

    return final_out


def train_neural_network(x):
    # Run the input vector (tensor) through the neural network.
    prediction = neural_network_model(x)

    # Use the cross entropy function on the prediction to obtain the loss.
    # (Softmax normalizes the entries in the output vector such that their sum
    # is one.)
    cost = tf.reduce_mean(
        tf.nn.softmax_cross_entropy_with_logits_v2(
            logits=prediction, labels=y))

    # Select an optimizer for backpropagation (here we chose Adam).
    optimizer = tf.train.AdamOptimizer().minimize(cost)

    # Epoch = feed forward + backpropagation
    num_epochs = 10

    with tf.Session() as sess:
        # Initialize all the session variables.
        sess.run(tf.initialize_all_variables())

        # Train the network on the training data.
        for epoch in xrange(num_epochs):
            epoch_loss = 0
            for _ in range(int(mnist.train.num_examples / batch_size)):
                epoch_x, epoch_y = mnist.train.next_batch(batch_size)
                _, c = sess.run([optimizer, cost],
                                feed_dict={x: epoch_x,
                                           y: epoch_y})
                epoch_loss += c
            print "Epoch {} complete out of {}. Loss: {}".format(
                epoch, num_epochs, epoch_loss)

        # Check that the same index is one-hot in the prediction and in the
        # actual label.
        correct = tf.equal(tf.argmax(prediction, 1), tf.argmax(y, 1))
        accuracy = tf.reduce_mean(tf.cast(correct, 'float'))
        print 'Accuracy: {}'.format(
            accuracy.eval({
                x: mnist.test.images,
                y: mnist.test.labels
            }))


train_neural_network(x)