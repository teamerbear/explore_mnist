import tensorflow as tf

x1 = tf.constant([1,2,3])
x2 = tf.constant([[1,2,3],[4,5,6],[7,8,9]])

# Simple way
#simple = x1 * x2

# The right "TensorFlow" way
result = tf.multiply(x1, x2)

# At this point, it is an abstract tensor.
print(result)

# To see the result, we have to actually run the session.
with tf.Session() as sess:
    print(sess.run(result))
